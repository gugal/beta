- Fixed the search box showing string IDs instead of strings.

Changes in 0.5 preview 2:
- Added an about page, replacing multiple settings items.
- Added Norwegian Bokmål translation (@kingu).
- Fixed the change list screen.
- Fixed the version text in the Settings page being black.
- On Android 11 and earlier Gugal uses a light blue color scheme instead of a purple and turquoise one.

For developers:
- This release contains some breaking changes to SERP providers. To find out what has changed see the development documentation at gugal.gitlab.io/dev-docs.html.

Known issues:
- You can finish set-up without entering credentials.

This is a preview version of Gugal. You can report bugs by making issues on Gugal's GitLab repository (make sure to add the "beta" label).
