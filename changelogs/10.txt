App:
- Added a top bar to the search page.
- The search widget has been redesigned.
- When an error occurs with a search engine a special "no results" screen is shown.
- When the credentials of a search engine are incorrect search set-up is automatically shown.

Localization:
- Added Turkish translation (metezd).
- Added partial Simplified Chinese translation (Hugel).
- Added partial French translation (J. Lavoie).

This is a preview version of Gugal. You can report bugs by making issues on Gugal's GitLab repository (make sure to add the "beta" label).